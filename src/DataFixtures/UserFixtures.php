<?php
namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     * @throws \App\Model\Api\ApiException
     */
    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => 'kkk@kkk.ru',
            'passport' => 'passport 111',
            'password' => '220736aedcb18d7aa947a3a6a16e3fa9de32581feb95be1af61968eadece4c15',
        ], false);

        $manager->persist($user);
        $manager->flush();
    }
}
